use std::process::Command;
use std::env;
use std::io::stdout;
use std::fs;
use ferris_says::say; // from the previous step
use std::io::{BufWriter};
use fs_extra::dir::create;
use std::env::args;
use std::process::exit;
use std::path::Path;
use std::fs::File;
extern crate fs_extra;
use fs_extra::dir::copy;
use fs_extra::dir::CopyOptions;
use fs_extra::file::CopyOptions as OtherCopyOptions;


fn main() {
	let args: Vec<String> = env::args().collect();
	let fonctionalite = &args[1];

	if fonctionalite == "clear" {
		cleaner();
		exit(0x0100);
	}

	if fonctionalite == "history" {
		history();
		exit(0x0100);
	} 
	
	if fonctionalite == "pkglist" {
		pkglist();
		exit(0x0100);
	}
	

	if fonctionalite == "install" {
		install();
		exit(0x0100);
	}
	if fonctionalite == "repolist" {
		repolist(); 
		exit(0x0100);
	}

	if fonctionalite == "help" {
		help();
		exit(0x0100);
	}
	
	if fonctionalite == "installed" {
		pkglistall();
		exit(0x0100);
	}

	if fonctionalite == "remove_conf" {
		remove_conf();
		exit(0x0100);
	}
	
	if fonctionalite == "remove" {
		remove();
		exit(0x0100);
	}
	if fonctionalite == "fix" {
		fix();
		exit(0x0100);
	}

	if fonctionalite == "local" {
		update_local();
		exit(0x01100);
	}
}

fn help() {
	println!("l'argument help permet d'afficher de l'aide");
	println!("Pour supprimer un paquet : sudo/gksu deep remove paquet_à supprimer");
	println!("Pour supprimer un paquet avec sa config : sudo/gksu remove_confg paquet_à_supprimer user"); 
	println!("Pour clean le système : sudo/gksu clear");
	println!("Pour réinitialiser deep utilisez deep !");
	println!("Pour installer un paquet : sudo/gksu install nom_du_rpm");
	println!("Pour lister les paquets installés avec deep : pkglist");
	println!("Pour lister les paquets installés : sudo/gksu installed");
}

fn history() {
	let log = "/var/log/deep/deep.log";
	let data = fs::read_to_string(log).expect("Unable to read file");
    println!("{}", data)
}

fn pkglist() {
	println!("Les paquets installés avec deep sont, certains de ces paquets ne sont peut être plus installé ! : ");
	let pkgslist = "/var/list/pkgs";
    let data = fs::read_to_string(pkgslist).expect("Unable to read file");
    println!("{}", data);
	println!("Pour consulter la liste des paquets installé sur votre système, lancez la commande pkglistall");
}

fn pkglistall() {
	let output = Command::new("rpm")
            .arg("-qa")
            .output().unwrap_or_else(|e| {
                panic!("failed to execute process: {}", e)
     });
        let list = String::from_utf8_lossy(&output.stdout);
        println!("{}", list);
}

fn remove_conf(){
	let args: Vec<String> = env::args().collect();
	let action = &args[1];
	let packagelog = &args[2];
	let user = &args[3];
	let userlog = Path::new("/var/log/deep/");
    assert!(env::set_current_dir(&userlog).is_ok());
	let log = "/var/log/deep/deep.log";
    let datalog = fs::read_to_string(log).expect("Unable to read file");
    println!("Mise à jour des bases de données {}", datalog);
	let newlog = format!("{} {} {}", datalog, action, packagelog);
	fs::write("/var/log/deep/deep.log", newlog);
	let args: Vec<String> = env::args().collect();
    let package = &args[2];
    println!("Nous nous occupons de delete votre paquet !"); std::process::Command::new("rpm").arg("-e").arg("--nodeps").arg(package).output().expect("Failed"); 
    let bin = Path::new("/usr/bin/");
    assert!(env::set_current_dir(&bin).is_ok());
    println!("Nous vérifions : {}!", bin.display());
    fs::remove_dir_all(package);
    let lib = Path::new("/usr/lib/");
    assert!(env::set_current_dir(&lib).is_ok());
    println!("Nous vérifions : {}!", lib.display());
    fs::remove_dir_all(package);
    println!("Le paquet {} a été delete", package);
    println!("Nous nettoyons les résidus ...");
    let tmp = Path::new("/tmp/");
    assert!(env::set_current_dir(&tmp).is_ok());
    println!("Nous vidons le cache {}!", tmp.display());
    File::create("lock");
    let options = CopyOptions::new();
    copy("/var/cache/apt/archives/partial", "/tmp/", &options);
    fs::copy("/var/cache/apt/archives/lock", "/tmp/lock");
    let archives = Path::new("/var/cache/apt/");
    assert!(env::set_current_dir(&archives).is_ok());
    println!("Fait : {}!", archives.display());
    fs::remove_dir_all("archives");
    fs::create_dir("archives");
    let archives = Path::new("/var/cache/apt/archives");
    assert!(env::set_current_dir(&archives).is_ok());
    println!("Copy de lock et partial {}!", archives.display());
    File::create("lock");
    fs::copy("/tmp/lock", "lock");
    copy("/tmp/partial", "/var/cache/apt/archives", &options);
 	let home = Path::new("/home/");
   	assert!(env::set_current_dir(&home).is_ok());
	let mut config = format!("/home/{}/.config/{}", user, package);
   	fs::remove_dir_all(config);
}

fn remove() {
let args: Vec<String> = env::args().collect();
	let action = &args[1];
	let packagelog = &args[2];
	let user = &args[3];
	let userlog = Path::new("/var/log/deep/");
    assert!(env::set_current_dir(&userlog).is_ok());
	let log = "/var/log/deep/deep.log";
    let datalog = fs::read_to_string(log).expect("Unable to read file");
    println!("Mise à jour des bases de données {}", datalog);
	let newlog = format!("{} {} {}", datalog, action, packagelog);
	fs::write("/var/log/deep/deep.log", newlog);
	let args: Vec<String> = env::args().collect();
    let package = &args[2];
    println!("Nous nous occupons de delete votre paquet !"); std::process::Command::new("rpm").arg("-e").arg("--nodeps").arg(package).output().expect("Failed"); 
    let bin = Path::new("/usr/bin/");
    assert!(env::set_current_dir(&bin).is_ok());
    println!("Nous vérifions : {}!", bin.display());
    fs::remove_dir_all(package);
    let lib = Path::new("/usr/lib/");
    assert!(env::set_current_dir(&lib).is_ok());
    println!("Nous vérifions : {}!", lib.display());
    fs::remove_dir_all(package);
    println!("Le paquet {} a été delete", package);
    println!("Nous nettoyons les résidus ...");
    let tmp = Path::new("/tmp/");
    assert!(env::set_current_dir(&tmp).is_ok());
    println!("Nous vidons le cache {}!", tmp.display());
    File::create("lock");
    let options = CopyOptions::new();
    copy("/var/cache/apt/archives/partial", "/tmp/", &options);
    fs::copy("/var/cache/apt/archives/lock", "/tmp/lock");
    let archives = Path::new("/var/cache/apt/");
    assert!(env::set_current_dir(&archives).is_ok());
    println!("Fait : {}!", archives.display());
    fs::remove_dir_all("archives");
    fs::create_dir("archives");
    let archives = Path::new("/var/cache/apt/archives");
    assert!(env::set_current_dir(&archives).is_ok());
    println!("Copy de lock et partial {}!", archives.display());
    File::create("lock");
    fs::copy("/tmp/lock", "lock");
    copy("/tmp/partial", "/var/cache/apt/archives", &options);
}

fn update_local() {
	let args: Vec<String> = env::args().collect();
	let package_old = &args[2];
	let package_new = &args[3];
	std::process::Command::new("rpm").arg("-e").arg(package_old).output().expect("Failed");
	let path = fs::read_to_string("/var/deep/path").expect("Failed");
	let file = format!("{}/{}", path, package_new);
	std::process::Command::new("rpm").arg("-i").arg(file).spawn();
	println!("Done");
	std::process::Command::new("echo").arg("hello").spawn();
	exit(0x01100);
}

fn download() {
	let args: Vec<String> = env::args().collect();
	println!("comming soon");
	let package = &args[2];
	let mirror = "/etc/deep/mirror/mirrorlist";
	let contenu = fs::read_to_string(mirror)
        .expect("Quelque chose s'est mal passé lors de la lecture du fichier");
	let repo = format!("{}/", contenu);
	let package1 = format!("{}RPMS.kde5/{}", contenu, package);
	let package2 = format!("{}RPMS.mate/{}", contenu, package);
	let package3 = format!("{}RPMS.x86_64/{}", repo, package);
	let package4 = format!("{}RPMS.xfce4/{}", contenu, package);
	let package5 = format!("{}base/{}", contenu, package);
	std::process::Command::new("wget").arg(package1).current_dir("/var/cache/deep").output().expect("Failed");
	std::process::Command::new("wget").arg(package2).current_dir("/var/cache/deep").output().expect("Failed");
	std::process::Command::new("wget").arg(package3).current_dir("/var/cache/deep").output().expect("Failed");
	std::process::Command::new("wget").arg(package4).current_dir("/var/cache/deep").output().expect("Failed");
	std::process::Command::new("wget").arg(package5).current_dir("/var/cache/deep").output().expect("Failed");
}

fn pkglistcreate() {
	let args: Vec<String> = env::args().collect();
	let thelist = &args[2];
	let pkgslist = "/var/list/pkgs";
    let data = fs::read_to_string(pkgslist).expect("Unable to read file");
    println!("Nous inscrivons votre paquet dans nos bases de données {}", data);
	let createlist = format!("{} {}", data, thelist);
	fs::write("/var/list/pkgs", createlist);
} 

fn install() {
	println!("Attention ! Deep n'étant pas en mesure de résoudre les dépendances, il vous faudra installer celles-ci manuellement !");
	let args: Vec<String> = env::args().collect();
	let action = &args[1];
	let packagelog = &args[2];
	let log = Path::new("/var/log/deep/");
    assert!(env::set_current_dir(&log).is_ok());
	let actuallog = "/var/log/deep/deep.log";
    let datalog = fs::read_to_string(actuallog).expect("Unable to read file");
    println!("Mise à jour des bases de données {}", datalog);
	let newlog = format!("{} {} {}", datalog, action, packagelog);
	fs::write("/var/log/deep/deep.log", newlog);
	download();
	pkglistcreate();
	let cache = Path::new("/var/cache/deep/");
    assert!(env::set_current_dir(&cache).is_ok());
	std::process::Command::new("rpm").arg("-i").arg(packagelog).output().unwrap();
}

fn repolist() {
	let repofile = "/etc/deep/mirror/mirrorlist";
	let repo = fs::read_to_string(repofile).expect("failed");
	println!("Les rpms sont téléchargés depuis ce dépôt : {}", repo);
}

fn fix() {
	let args: Vec<String> = env::args().collect();
	let distro = &args[2];
	let stdout = stdout();

	if distro == "fedora" {
		fs::remove_file("/etc/deep/id");
		let fedora = format!("{}", distro);
		File::create("/etc/deep/id");
		fs::write("/etc/deep/id", fedora);
		fs::create_dir("/etc/deep/");
		fs::create_dir("/etc/deep/mirror");
		File::create("/etc/deep/mirror/mirrorlist");
		let mirror = "http://ftp.nluug.nl/pub/os/Linux/distr/pclinuxos/pclinuxos/apt/pclinuxos/64bit";
		fs::write("/etc/deep/mirror/mirrorlist", mirror);

	} else {
		fs::remove_file("/etc/deep/id");
		File::create("/etc/deep/id");
		let pc = "pclinuxos";
		fs::write("/etc/deep/id", pc);
		fs::create_dir("/etc/deep/");
		fs::create_dir("/etc/deep/mirror");
		File::create("/etc/deep/mirror/mirrorlist");
		let mirror = "http://ftp.nluug.nl/pub/os/Linux/distr/pclinuxos/pclinuxos/apt/pclinuxos/64bit";
		fs::write("/etc/deep/mirror/mirrorlist", mirror);
	}
 
    let message = String::from("Deep est en place :-)  !");
    let width = message.chars().count();
    let mut writer = BufWriter::new(stdout.lock());
    say(message.as_bytes(), width, &mut writer).unwrap();
	// rajouter un warning 
	let log = Path::new("/var/log/");
    assert!(env::set_current_dir(&log).is_ok());
    println!("Nous mettons en place les deep.log {}!", log.display());
	fs::remove_dir_all("deep");
	fs::create_dir("deep");
	let logcreate = Path::new("/var/log/deep/");
    assert!(env::set_current_dir(&logcreate).is_ok());
    println!("Done {}!", logcreate.display());
	File::create("deep.log");
	let deeplog = "deep first log";
	fs::write("deep.log", deeplog);
	let list = Path::new("/var/");
    assert!(env::set_current_dir(&list).is_ok());
    println!("Nous mettons en place la liste des packages deep {}!", list.display());
	fs::create_dir("list");
	let listcreate = Path::new("/var/list/");
    assert!(env::set_current_dir(&listcreate).is_ok());
    println!("Done {}!", listcreate.display());
	File::create("pkgs");
	fs::create_dir("/var/cache/deep/");
	println!("Done ! ");
	fs::create_dir("/etc/deep/");
	fs::create_dir("/etc/deep/mirror");
	File::create("/etc/deep/mirror/mirrorlist");
	let mirror = "http://ftp.nluug.nl/pub/os/Linux/distr/pclinuxos/pclinuxos/apt/pclinuxos/64bit";
	fs::write("/etc/deep/mirror/mirrorlist", mirror);
	fs::remove_dir_all("/var/deep");
	fs::create_dir("/var/deep");
	File::create("/var/deep/path");
	let local = "/var/deep/update";
	fs::create_dir("/var/deep/update");
	fs::write("/var/deep/path", local);
}

fn cleaner() {
	let args: Vec<String> = env::args().collect();
	let action = &args[1];
	fs::remove_dir_all("/var/cache/deep");
	let userlog = Path::new("/var/log/deep/");
    assert!(env::set_current_dir(&userlog).is_ok());
	let actuallog = "/var/log/deep/deep.log";
    let datalog = fs::read_to_string(actuallog).expect("Unable to read file");
    println!("Mise à jour des bases de données {}", datalog);
	let newlog = format!("{} {}", datalog, action);
	fs::write("/var/log/deep/deep.log", newlog);
	println!("Nous nettoyons les résidus ...");
    let tmp = Path::new("/tmp/");
    assert!(env::set_current_dir(&tmp).is_ok());
    println!("Nous vidons le cache {}!", tmp.display());
    File::create("lock");
    let options = CopyOptions::new();
	let distro = fs::read_to_string("/etc/deep/id").expect("Failed");
	if distro == "pclinuxos" {
    	copy("/var/cache/apt/archives/partial", "/tmp/", &options);
    	fs::copy("/var/cache/apt/archives/lock", "/tmp/lock");
    	let archives = Path::new("/var/cache/apt/");
    	assert!(env::set_current_dir(&archives).is_ok());
    	println!("Fait : {}!", archives.display());
    	fs::remove_dir_all("archives");
    	fs::create_dir("archives");
    	let archives = Path::new("/var/cache/apt/archives");
    	assert!(env::set_current_dir(&archives).is_ok());
    	println!("Copy de lock et partial {}!", archives.display());
    	File::create("lock");
    	fs::copy("/tmp/lock", "lock");
    	copy("/tmp/partial", "/var/cache/apt/archives", &options);
	} 
	
	if distro == "fedora" {
		std::process::Command::new("dnf").arg("clean").arg("all").output().expect("failed");
		std::process::Command::new("dnf").arg("autoremove").output().expect("failed");
	}
		
	fs::create_dir("/var/cache/deep");
	println!("Done !");
}

