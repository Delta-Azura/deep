# Deep
Deep est un projet de gestionnaire de paquet. Tout cela est expérimental ! Le gestionnaire est très basique ( KISS ). Il n'a pas pour but d'être utilisé en production ou autre.

# Avantages inconvénients
Avantages :
- Très léger
- Rapidité d'éxécution lié à Rust

Inconvénients : 
- Uniquement ajout et suppression de logiciels ( donc chiant lorsque vous voulez installer un paquet ! ) 
- Très basique sans aucune perspective de personnalisation
- Aucune gestion des dépendances 

# Plus d'info
Pour plus d'information vous pouvez me contacter sur mon groupe telegram : https://t.me/joinchat/MVtY0ZKRwqNhNTI0 
